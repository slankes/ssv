---
title: "Bezirks-Schiessen 2022"
subtitle: ""
date: 2022-11-02T20:54:47+01:00
lastmod: 2022-11-02T20:54:47+01:00
draft: false
author: ""
authorLink: ""
description: ""
license: ""
images: []

tags: []
categories: ["Bezirks-Schiessen"]

featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: false
hiddenFromSearch: false
twemoji: false
lightgallery: true
ruby: true
fraction: true
fontawesome: true
linkToMarkdown: true
rssFullText: false

toc:
  enable: true
  auto: true
code:
  copy: true
  maxShownLines: 50
math:
  enable: false
  # ...
mapbox:
  # ...
share:
  enable: true
  # ...
comment:
  enable: true
  # ...
library:
  css:
    # someCSS = "some.css"
    # located in "assets/"
    # Or
    # someCSS = "https://cdn.example.com/some.css"
  js:
    # someJS = "some.js"
    # located in "assets/"
    # Or
    # someJS = "https://cdn.example.com/some.js"
seo:
  images: []
  # ...
---

<!--more-->

Im November 2022 fand nach zweijähriger Corona-Unterbrechung wieder das Bruderschaftspokalschiessen statt. Die Ergebnisse sind hier aufgelistet:

## Schüler

{{< embed-pdf url="/pdf/2022-pokalschiessen-BS_Schueler.pdf" >}}

## Jugend

{{< embed-pdf url="/pdf/2022-pokalschiessen-BS_Jugend.pdf" >}}

## Schützen

{{< embed-pdf url="/pdf/2022-pokalschiessen-BS_Schuetzen.pdf" >}}

## Sportschützen

{{< embed-pdf url="/pdf/2022-pokalschiessen-BS-Sportschuetzen.pdf" >}}

## Altersschützen

{{< embed-pdf url="/pdf/2022-pokalschiessen-BS_Altersschuetzen.pdf" >}}

## Jugendwanderpokal

{{< embed-pdf url="/pdf/2022-pokalschiessen-Jugendwanderpokal.pdf" >}}

## Pokal der Königshäuser

{{< embed-pdf url="/pdf/2022-pokalschiessen-Pokal_de_Koenigshaeuser.pdf" >}}

