---
title: "Vereinsmeisterschaften 2023"
subtitle: ""
date: 2023-01-01T20:51:34+01:00
lastmod: 2023-01-01T20:51:34+01:00
draft: false
author: ""
authorLink: ""
description: ""
license: ""
images: []

tags: []
categories: [Ankündigung]

featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: false
hiddenFromSearch: false
twemoji: false
lightgallery: true
ruby: true
fraction: true
fontawesome: true
linkToMarkdown: true
rssFullText: false

toc:
  enable: true
  auto: true
code:
  copy: true
  maxShownLines: 50
math:
  enable: false
  # ...
mapbox:
  # ...
share:
  enable: true
  # ...
comment:
  enable: true
  # ...
library:
  css:
    # someCSS = "some.css"
    # located in "assets/"
    # Or
    # someCSS = "https://cdn.example.com/some.css"
  js:
    # someJS = "some.js"
    # located in "assets/"
    # Or
    # someJS = "https://cdn.example.com/some.js"
seo:
  images: []
  # ...
---

<!--more-->

Am 16. und 17. Janaur finden die Vereinsmeisterschaften im Schützenheim statt:

 * Montag, 16.01.2022 ab 19:00 Uhr - Luftgewehr Freihand und LP 15/40 Schuss
 * Dienstag, 17.01.2022 ab 19:00 - Luftgewehr Aufgelegt 15/30 Schuss

Bitte geb bei der Vereinsmeisterschaft an, ob Du an weiterführenden Meisterschaften (Diözesan- / Bundesmeisterschaft) teilnehmen möchtest.

Bei Fragen wendet Euch an Frank Schubert


