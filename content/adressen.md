---
title: "Schießstände im Sportkreis Erkelenz"
subtitle: ""
date: 2022-11-02T20:59:10+01:00
lastmod: 2022-11-02T20:59:10+01:00
draft: false
author: "Sven Lankes"
authorLink: ""
description: ""
license: ""
images: []

tags: []
categories: []

featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: false
hiddenFromSearch: false
twemoji: false
lightgallery: true
ruby: true
fraction: true
fontawesome: true
linkToMarkdown: true
rssFullText: false

toc:
  enable: true
  auto: true
code:
  copy: true
  maxShownLines: 50
math:
  enable: false
  # ...
mapbox:
  # ...
share:
  enable: true
  # ...
comment:
  enable: true
  # ...
library:
  css:
    # someCSS = "some.css"
    # located in "assets/"
    # Or
    # someCSS = "https://cdn.example.com/some.css"
  js:
    # someJS = "some.js"
    # located in "assets/"
    # Or
    # someJS = "https://cdn.example.com/some.js"
seo:
  images: []
  # ...
---

<!--more-->
| Ort              | Bruderschaft        | Straße                | Ort / Bemerkung             | Telefon      |
|------------------|---------------------|-----------------------|-----------------------------|--------------|
| Aphoven          | St. Arnoldi         | Heideweg 40           | 52525 Heinsberg - Aphoven   |              |
| Baal             | St.Hubertus         | Bahnstr.13            | neben dem Bahnhof           |              |
| Beeck            | St.Sebastianus      | Holtumerstr.          | Grundschule unter Turnhalle |              |
| Birgelen         | St.Lambertus        | Elsumer Weg 6         | Schule, über der Turnhalle  |              |
| Dalheim Rödgen 1 | St.Rochus           | Dechant Rupertzhoven  | Jugendheim kath.Kirche      |              |
| Dalheim Rödgen 2 | St.Rochus           | An der Landwehr       | Schützenheim                |              |
| Doveren          | St.Sebastianus      | Dionysiusstr.10       | Pfarrheim                   | 02433 938729 |
| Erkelenz         | unserer lieben Frau | Westpromenade         | Pfarrzentrum St.Lambertus   |              |
| Golkrath         | St.Stephanus        | Terreicken 52         | Pfarrheim                   |              |
| Hetzerath        | St.Josef            |                       | neben der Kirche            |              |
| Hilfahrt         | St.Marien           | Nohlmannstr.36        | Pfarrheim                   |              |
| Hückelhoven      | v.hl.Sakr.d Altares | Dinstühlerstr.51      | Haus Schnorrenberg          |              |
| Kückhoven        | St.Sebastianus      | im Bonental           | hinter Mehrzweckhalle       |              |
| Lövenich         | St.Sebastianus      | Dingbuchenweg         | Mehrzweckhalle              |              |
| Millich          | St.Johannes         | Schützenwinkel 2      | 41836 Hückelhoven           |              |
| Myhl             | St.Johannes         | Am Schwanderberg      |                             |              |
| Niederkrüchten   | St.Antonius         | Mühlrather Hof        | 41366 Schwalmtal            |              |
| Rath-Anhoven     | St.Rochus           | Rochusstr.            | im Keller der Grundschule   |              |
| Ratheim          | St.Sebastianus      | Am Kirchberg          | an der Kirche               |              |
| Rickelrath       | St.Matthias         | Dülkenerstr. 73       | hinterm Chinarestaurant     |              |
| Steinki.-Effeld  | St.Martini          | Bruchstr.5            |                             |              |
| Tüschenbroich    | St.Lambertus,       | hinter der Kirche     |                             | 02434 1815   |
| Wegberg          | St.Antonius         | Maaseiker Str.        | Schulzentrum                | 02434 24242  |
| Wassenberg       | St.Marien           | Am Stadtrain          | neben der Kirche            |              |

Korrekturen und Erweiterungen bitte per Email an sven+schiessstaende@lank.es
